<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Thread_model extends CI_Model{

	function insert_data($data){

		$this->load->database();
		$this->db->insert('content_thread', $data);

	}

	function insert_comment($data){

		$this->load->database();
		$this->db->insert('comment', $data);
	}

	function select_data(){

		$this->load->database();
		$this->db->select('thread_id, thread_title');
		$query = $this->db->get('content_thread');
		return $query;
	}

	function view_comment($id = null){
		$this->load->database();
		$this->db->select('comment_id, comment, thread_id');
		if(isset($id))$this->db->where('thread_id', $id);
		$query = $this->db->get('comment');	

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else{
			return FALSE;
		}
	}

	function get_data($id = null){
		$this->load->database();
		$this->db->select('thread_id, thread_title');
		if(isset($id))$this->db->where('thread_id', $id);
		$query = $this->db->get('content_thread');

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else{
			return FALSE;
		}
	}

	function update_data($comment_id){
		//load db nya
		$this->load->database();
		//select datanya dari database
		$this->db->select('comment_id, comment');
		if(isset($comment_id))$this->db->where('comment_id', $comment_id);
		//get query
		$query = $this->db->get('comment');
		return $query->result();
	}
}