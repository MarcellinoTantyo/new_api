<div class="container">
	<div class="title">
		Thread Hot Topic
	</div>

	<div class="body">
		<div class="content">
			<form action="<?php echo site_url('Thread/thread_title'); ?>" method="post")>
				<div class="form-group">
					<label for="inputThreadTitle">Thread Title</label>
					<input type="text" class="form-control" name="threadtitle" id="threadtitle" placeholder="Input thread title here...">
					<span class="text-danger"><?php echo form_error("threadtitle"); ?></span>
				</div>
				<button type="submit" class="btn btn-success" name="submit" value="Submit">Submit</button>
			</form>
		</div>
	</div>
</div>