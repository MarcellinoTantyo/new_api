<div class="container">
	<div class="title">
		Thread List
	</div>

	<div class="content">

		<?php 
		if($select_data->num_rows() > 0){
			foreach ($select_data->result() as $row) {
		?>
				<ul>
					<li><a href="<?php echo site_url('Thread/thread_comment/') ?><?php echo $row->thread_id; ?>"><?php echo $row->thread_title; ?></a></li>
				</ul>
		<?php
			}
		}else{
			echo "No data found...";
		}
		?>

	</div>
</div>