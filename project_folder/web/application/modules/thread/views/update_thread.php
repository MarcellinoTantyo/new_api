<div class="title">
	Update
</div>

<div class="container">
	<div class="content">
	<form action="<?php echo site_url('Thread/update_data/'.$update_data[0]->comment_id); ?>" method="post">
		<div class="form-group">
			<label for="editComment">Comment :</label>
			<input type="text" name="editComment" class="form-control" value="<?php echo($update_data[0]->comment) ?>">
			<br>
			<button type="submit" class="btn btn-info" value="update">Update</button>
		</div>
	</form>
	</div>
</div>