<div class="container">
	<div class="title">
		Thread Comment
	</div>

	<div class="body">
		<div class="content">
			<form action="<?php echo site_url('Thread/thread_comment/'.$id); ?>" method="post">
				<div class="form-group">
					<?php 
					
					if(!empty($get_data)){
						foreach ($get_data as $row) {
							?>
							<label for="threadTitle">Thread Title :<?php echo $row->thread_title; ?></label>
							<input type="text" name="inputcomment" id="inputcomment" class="form-control">
							<?php
						}
					}else{
						echo "No data found...";
					}
					?>
					<br>
					<button type="submit" name="comment" value="Comment" class="btn btn-success">Comment</button>
				</div>
			</form>
			
			<label for="comment">Comment :</label>
			<?php 
			if(!empty($view_comment)){
				foreach ($view_comment as $row) {
			?>
					<ul>
						<li><?php echo $row->comment; ?>
							<a href="<?php echo site_url('Thread/update_data/'.$row->comment_id); ?>"><button type="button" class="btn btn-info">Edit</button></a>
						</li>
					</ul>
			<?php
				}
			}else{
				echo "No comment...";
			}
			?>

		</div>
	</div>
</div>