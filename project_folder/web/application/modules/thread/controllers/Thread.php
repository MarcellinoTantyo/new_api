<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Thread extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


		function __construct(){
			parent::__construct();

			$this->load->helper(array('form', 'url'));

			$this->load->library('form_validation');

			$this->load->model("thread_model");
		}

		function thread_title(){

			$this->form_validation->set_rules('threadtitle', 'Thread Title', 'required|trim|alpha|min_length[5]|max_length[50]');

			$file["css"] = $this->load->view("template/css/base", null, true);

			if ($this->form_validation->run() == FALSE) {

				$file["body"] = $this->load->view("thread_title", null, true);	

			}else{

				$data = array(
					"thread_title"	=>$this->input->post("threadtitle")	
					);

				$this->thread_model->insert_data($data);

				$file["body"] = $this->load->view("success", null, true);
			}

			$file["js"] = $this->load->view("template/js/base", null, true);

			$this->load->view("template/one_col", $file);

		}

		function thread_list(){

			$file["css"] = $this->load->view("template/css/base", null, true);

			$data["select_data"] = $this->thread_model->select_data();

			$file["body"] = $this->load->view("thread_list", $data, true);

			$file["js"] = $this->load->view("template/js/base", null, true);

			$this->load->view("template/one_col", $file);
		}

		function thread_comment($id = null){

			$this->form_validation->set_rules('inputcomment', 'Comment', 'required|trim');

			$file["css"] = $this->load->view("template/css/base", null, true);

			$data["get_data"] = $this->thread_model->get_data($id);

			$data["view_comment"] = $this->thread_model->view_comment($id);

			$data["id"] = $id;

			if (count($data) > 0) {

				if ($this->form_validation->run() == FALSE) {

					$file["body"] = $this->load->view("thread_comment", $data, true);	

				}else{

					$data2 = array(

						"comment"	=>$this->input->post("inputcomment"),
						"thread_id"	=>$data["id"]	
						);

					$this->thread_model->insert_comment($data2);

					$data["view_comment"] = $this->thread_model->view_comment($id);

					$file["body"] = $this->load->view("thread_comment", $data, true);

				//$file["body"] = $this->load->view("success", null, true);
				}

				$file["js"] = $this->load->view("template/js/base", null, true);

				$this->load->view("template/one_col", $file);

				return $data;
			}
			else{
				return FALSE;
			}
		}

		function update_data($comment_id){

			$this->form_validation->set_rules('editComment', 'Thread Comment', 'required|trim|min_length[5]|max_length[50]');

			if ($this->form_validation->run() == FALSE) {
				echo(validation_errors());
				
			}else{
				$data = array(
					"comment"	=>$this->input->post("editComment")	
					);
				//var_dump($data);
			}

			$file["css"] = $this->load->view("template/css/base", null, true);

			$data["update_data"] = $this->thread_model->update_data($comment_id);
			$file["body"] = $this->load->view("update_thread", $data, true);

			$data = array(
				"comment" => $this->input->post("editComment")
			);

			$this->db->set($data);
			$this->db->where('comment_id', $comment_id);
			$this->db->update('comment');


			$file["js"] = $this->load->view("template/js/base", null, true);

			$this->load->view("template/one_col", $file);
		}
	}
	?>