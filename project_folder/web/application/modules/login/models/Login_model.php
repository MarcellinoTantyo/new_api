<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	/**
	 * check input password with hash
	 * @param  array $data user data(username, email, password from input, etc)
	 * @return string       check status
	 */
	public function check_password($data){
		//set password hash manually
		$hash = '$2y$10$QddKmXPiRiwat4TXPvRIO.MQiFu6Gdaq8ylxveITkZb9fOux4fEo2'; //hash for = test
		//or
		//get from database
		/*$where = array('username' => $data['username']);
		$userdata = $this->get_password_from_db($where);
		if(empty($userdata))return "user not found";
		var_dump($userdata);
		$hash = $userdata->password;*/

		//check hash data with inputted password
		if(!empty($data['password']) && !empty($hash)){
			if(password_verify($data['password'], $hash)){
				//password match
				
				//update session
				$session_data = array(

					);
				$this->session->set_userdata( $session_data );

				return "password match";
			}
			else{
				//password doesnt match
				
				//set session flashdata
				//$this->session->set_flashdata('login_msg', 'password doesn\'t match');

				return "password doesnt match";
			}
		}
		else{
			//password and hash required
			
			//set session flashdata
			//$this->session->set_flashdata('login_msg', 'password and hash required');
			
			return "password and hash required";
		}
	}

	public function check_password2(){
		
	}

	private function get_password_from_db($where){
		$this->db->select('password');
		$this->db->from('admin');
		$this->db->where($where);
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result()[0];
		}
		else{
			return false;
		}
	}

//====================================================================================================
	
	private function select($table_name, $table_field, $condition = null){
		$this->db->select($table_field);
		$this->db->from($table_name);
		if(isset($condition)){
			//where
			if(isset($condition['where']))$this->db->where($condition['where']);
			//limit
			if(isset($condition['limit'])){
				$limit = $condition['limit'];
				$offset = isset($condition['offset']) ? $condition['offset'] : 0;
				$this->db->limit($limit, $offset);
			}
		}

		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			if(isset($condition['limit'])){
				if($condition['limit'] == 1){
					return $query->result()[0];
				}
				else{
					return $query->result();
				}
			}
			return $query->result();
		}
		else{
			return false;
		}
	}

	private function update($table_name, $update_data, $where){
		$this->db->trans_start();
		
		$this->db->where($where);
		$this->db->update($table_name, $update_data);

		$this->db->trans_complete();
		
		if($this->db->affected_rows() > 0)return true;
		else return false;
	}

	private function delete($table_name, $where){
		$this->db->trans_start();
		
		$this->db->where($where);
		$this->db->delete($table_name);

		$this->db->trans_complete();

		if($this->db->affected_rows() > 0) return true;
		else return false;
	}
}