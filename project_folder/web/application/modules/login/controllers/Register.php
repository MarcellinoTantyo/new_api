<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MX_Controller{

	function __construct(){
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('auth/ion_auth');
		
		// $config = $this->load->config('auth/ion_auth', true);
		// var_dump($config);
	}

	public function index(){
		var_dump($this->ion_auth->logged_in());
		$this->register_page();
	}

	private function register_page(){
		// echo "test";
		echo modules::run('auth/register');
	}
}