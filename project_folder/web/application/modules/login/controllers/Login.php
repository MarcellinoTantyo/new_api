<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();
		//$this->load->library('ion_auth');
		$config = $this->load->config('auth/ion_auth');
		var_dump($config);
	}

	function index(){
		$this->login_page();
	}

	private function login_page(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'username', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('password', 'password', 'trim|required|max_length[20]');

		if($this->form_validation->run() == FALSE){
			$this->load->view('login');
		}
		else{
			$login_data['username'] = $this->input->post('username');
			$login_data['password'] = $this->input->post('password');
			$this->do_login($login_data);
		}
	}

	//PRIVATE FUNCTION
	private function do_login($login_data){
		var_dump($login_data);
		//$this->load->model('login_model');
		$this->load->library('auth/ion_auth');
		// $this->ion_auth->logout();
		$this->ion_auth->login($login_data['username'], $login_data['password'], false);
		//echo $this->login_model->check_password($login_data);
	}
}
?>