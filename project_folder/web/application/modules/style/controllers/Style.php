<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Style extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();
	}

	function index(){
		
		$file["css"] = $this->load->view("template/css/base", null, true);

		$file["body"] = $this->load->view("style", null, true);

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);
	}
}
?>