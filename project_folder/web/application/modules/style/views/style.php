<nav class="navbar navbar-default navbar1">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">
				<a href="#"><img src="<?php echo base_url('asset/images/logo.svg'); ?>" alt="Twine"></a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<!-- <ul class="nav navbar-nav">
				
			</ul> -->
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">About</a></li>
				<li><a href="#">We're Hiring</a></li>
			</ul>
		</div>
	</div>
</nav>



<div class="content1">
	Work on <br> real problems.
</div>

<div class="content2">
	We're looking for hard-working <br>
	fun-loving people to join us in <br>
	San Francisco
</div>

<div class="square"></div>
<div class="border"></div>
<div class="border1"></div>