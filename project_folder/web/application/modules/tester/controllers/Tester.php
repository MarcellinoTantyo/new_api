<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tester extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->load->model("tester_model");
	}

	function register(){

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|max_length[12]');
		$this->form_validation->set_rules('cpassword', 'Confirmation Password', 'required|trim|min_length[6]|max_length[12]|matches[password]');
		$this->form_validation->set_rules('province', 'Province', 'required|trim');

		$file["css"] = $this->load->view("template/css/base", null, true);

		$data["select_data"] = $this->tester_model->select_data();

		if ($this->form_validation->run() == false) {
				
			$file["body"] = $this->load->view("register", $data, true);
		}else{

			$data = array(
				"email"		=> $this->input->post("email"),
				"name"		=> $this->input->post("name"),
				"password"	=> $this->input->post("password"),
				"province"	=> $this->input->post("province")
			);

			$this->tester_model->insert_data($data);

			$file["body"] = $this->load->view("success", null, true);
		}

		$file["js"] = $this->load->view("template/js/base");

		$this->load->view("template/one_col", $file);
	}

	function login(){

		$this->form_validation->set_rules('email', 'Email', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		$file["css"] = $this->load->view("template/css/base", null, true);

		if ($this->form_validation->run() == false) {

			$file["body"] = $this->load->view("login", null, true);

		}else{

			$email = $this->input->post('email');
			$password = $this->input->post('password');
			
			if ($a = $this->tester_model->login($email, $password)) {
				$session_data = array(
					'id'	=> $a[0]->id,
					'email'	=> $a[0]->email
				);
				$this->session->set_userdata($session_data);
				redirect(site_url('tester/home'));
			}else{
				$this->session->set_flashdata('error', 'Invalid Email or Password');
				redirect(site_url('tester/login'));
			}

		}

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);
	}

	function home(){

		$id = $this->session->userdata('id');
		
		$file["css"] = $this->load->view("template/css/base", null, true);

		$data["view_friend"] = $this->tester_model->view_friend($id);

		$data["friend_request"] = $this->tester_model->friend_request($id);
		$data["friend_compare"] = $this->tester_model->temp($id);
		$data["view_data"] = $this->tester_model->view_data($id);
		//$data["id"] = $id;

		if ($this->session->userdata('email') != '') {
			
			$file["body"] = $this->load->view("home", $data, true);

			//$data["view_data"] = $this->tester_model->view_data($id);

			//$data["view_friend"] = $this->tester_model->view_friend();

			//var_dump($data);
			
		}else{
			redirect(site_url('tester/login'));
		}

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);
	}

	function session(){
		var_dump($this->session->userdata());
	}

	function logout(){

		$this->session->unset_userdata('email');
		$this->session->unset_userdata('id');
		redirect(site_url('tester/login'));
	}

	function add_friend($id){
		// $id dari button ketika di click
		$data = array(
			'user_id'	=> $this->session->userdata('id'),
			'friend_id'	=> $id,
			'status'	=> "0",
			'action'	=> $this->session->userdata('id')
		);

		$this->tester_model->insert_friend($data);
		$data = array(
			'user_id'	=> $id,
			'friend_id'	=> $this->session->userdata('id'),
			'status'	=> "0",
			'action'	=> $this->session->userdata('id')
		);

		$this->tester_model->insert_friend($data);
		//$data["status"] = $status;
		//var_dump($data);
		redirect('tester/home');
		}

/**
 * [accept_friend description]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	function accept_friend($action){

		$id = $this->session->userdata('id');

		$this->tester_model->accept_friend($id, $action);

		redirect('tester/home');

		// // $data['accept_friend'] = $this->tester_model->accept_friend($id, $action);

		// if ($this->session->userdata('id') < $this->tester_model->accept_friend()) {
		// 	$data = array(
		// 		'user_id'	=> $this->session->userdata('id'),
		// 		'friend_id'	=> $this->tester_model->accept_friend()
		// 	);
		// }else if($this->session->userdata('id') > $this->tester_model->accept_friend()){
		// 	$data = array(
		// 		'user_id'	=> $this->tester_model->accept_friend(),
		// 		'friend_id'	=> $this->session->userdata('id')
		// 	);
		// }
		// $this->tester_model->accept_friend($data);
	}
}

?>