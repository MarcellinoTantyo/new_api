<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tester_model extends CI_Model{

	function select_data(){
		$this->load->database();
		$this->db->select('province_id, province_name');
		$query = $this->db->get('province');
		return $query->result();
	}

	function insert_data($data){
		$this->load->database();
		$this->db->insert('user', $data);
	}

	function login($email, $password){
		$this->load->database();
		$this->db->select('id, email, name, password');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get('user');
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	function view_data($id = null){
		$this->load->database();
		$this->db->select('id, email, name, password, province_name');
		$this->db->join('province', 'province.province_id = user.province');
		if(isset($id))$this->db->where('id', $id);
		$query = $this->db->get('user');
		//var_dump($query->result());
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	function view_friend($id = null){
		
		$this->load->database();
		$this->db->select('id, name, province_name');
		$this->db->join('province', 'province.province_id = user.province','left');
		// $this->db->join('friends', ' friend_id = user.id', 'left');
		$this->db->where('id !=', $id);

		if(isset($id))$this->db->where('id !=', $id);

		$query = $this->db->get('user');

		// $this->db->join('friends s', 's.user_id = user.id', 'left');
		// $this->db->group_by("user.id");
		// $this->db->where('friends.user_id = user.id OR friends.friend_id = user.id');

		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	function temp($id){
		$this->load->database();
		$this->db->select('user_id,friend_id,status,action');
		$this->db->where('user_id', $id)->or_where('friend_id', $id);


		$friends = $this->db->get('friends');
		return $friends->result();
	}

	function insert_friend($data){
		$this->load->database();
		$this->db->insert('friends', $data);
	}

	function friend_request($id = null){
		$this->load->database();
		$this->db->select('id, name, user_id, friend_id, status, action');
		$this->db->join('user', 'user.id = friends.user_id', 'left');
		$this->db->where('status', 0);
		// $this->db->where('action !=', $id);
		$this->db->where('friend_id', $id);
		$this->db->where('action !=', $id);
		
		$query = $this->db->get('friends');
		//var_dump($query->result());
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	function accept_friend($id, $action){
		$this->load->database();
		// $this->db->select('user_id, friend_id, status, action');
		$this->db->set('status', 1);
		$this->db->set('action', $id);
		// $this->db->where('user_id', $action);
		// $this->db->or_where('friend_id', $action);
		$array_id = array($id, $action);
		$this->db->where_in('user_id', $array_id);
		$this->db->where_in('friend_id', $array_id);
		$this->db->update('friends');
	}
}