<div class="title">
	Login
</div>

<form action="<?php echo site_url('tester/login/'); ?>" method="post">
	<div class="form-group">
		<label for="email">Email :</label>
		<input type="email" name="email" class="form-control" placeholder="Email">
		<div class="text-danger"><?php echo form_error('email'); ?></div>
	</div>
	<div class="form-group">
		<label for="password">Password :</label>
		<input type="password" name="password" class="form-control" placeholder="Password">
		<div class="text-danger"><?php echo form_error('password'); ?></div>
	</div>
	<button type="submit" class="btn btn-primary">Login</button>
	<?php echo $this->session->flashdata("error"); ?>
</form>