<div class="title">
	Register
</div>

<form action="<?php echo site_url('tester/register/'); ?>" method="post">
	<div class="form-group">
		<label for="email">Email :</label>
		<input type="email" name="email" class="form-control" placeholder="Email">
		<div class="text-danger"><?php echo form_error('email'); ?></div>
	</div>
	<div class="form-group">
		<label for="name">Name :</label>
		<input type="text" name="name" class="form-control" placeholder="Name">
		<div class="text-danger"><?php echo form_error('name'); ?></div>
	</div>
	<div class="form-group">
		<label for="password">Password :</label>
		<input type="password" name="password" class="form-control" placeholder="Password">
		<div class="text-danger"><?php echo form_error('password'); ?></div>
	</div>
	<div>
		<label for="cpassword">Confirmation Password :</label>
		<input type="password" name="cpassword" class="form-control" placeholder="Confirmation Password">
		<div class="text-danger"><?php echo form_error('cpassword'); ?></div>
	</div>
	<br>
	<div class="form-group">
		<label for="province">Province :</label>
		<select name="province" class="form-control">
			<option class="hidden" value="">Choose province</option>
			<?php 
			if (!empty($select_data)) {
				foreach ($select_data as $row) {
			?>
				<option value="<?php echo $row->province_id; ?>"><?php echo $row->province_name; ?></option>
			<?php
				}
			}
			?>
		</select>
		<div class="text-danger"><?php echo form_error('province'); ?></div>
	</div>
	<button type="submit" class="btn btn-success">Register</button>
</form>