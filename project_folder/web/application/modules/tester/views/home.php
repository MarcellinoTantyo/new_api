<?php
if (!empty($view_data)) {
	foreach ($view_data as $row) {
		?>
		<label for="email">
			Email : <?php echo $row->email; ?>
		</label>
		<br>
		<label for="name">
			Name : <?php echo $row->name; ?>
		</label>
		<br>
		<label for="province">
			Province : <?php echo $row->province_name; ?>
		</label>
		<br>

		<?php
	}
}else{
	echo "Data Not Found...";
}
?>

<h3>Your form was successfully submitted!</h3>

<p><?php echo anchor('tester/logout', 'Go Logout Page!'); ?></p>

<div class="table">
	<table class="table table-striped">
		<tr>
			<th>Name</th>
			<th>Province</th>
			<th>Action</th>
		</tr>
		<?php
		$listOfFriends = array();
		$listOfStatus = array();
		foreach($friend_compare as $compare){
			if($view_data[0]->id == $compare->user_id){
				if (!in_array($compare->friend_id, $listOfFriends)){
					array_push($listOfFriends,$compare->friend_id);
					$listOfStatus[$compare->friend_id] = $compare->status;
				}
			}
		}
		if (!empty($view_friend)) {
			foreach ($view_friend as $row) {
				?>
				<tr>
					<td><?php echo $row->name; ?></td>
					<td><?php echo $row->province_name; ?></td>
					<?php 
					if (in_array($row->id, $listOfFriends)) {
				//var_dump($row);
						if ($listOfStatus[$row->id]){
							?>
							<td><?php echo "Already Friend"; ?></td>
							<?php
						}
						else {
							?>
							<td><?php echo "Pending Friend"; ?></td>
							<?php
						}
					}
					else{
						?>
						<td><a href="<?php echo site_url('tester/add_friend/'.$row->id); ?>"><button type="button" class="btn btn-primary">Add Friend</button></a></td>
					</tr>
					<?php
				}
			}
		}else{
		echo "Data Not Found...";
	}  
	?>
</table>
</div>
<div class="table2">
	<label for="">Friend Request</label>
	<table class="table table-striped">
		<tr>
			<th>Name</th>
			<th>Action</th>
		</tr>
		<?php
		if (!empty($friend_request)) {
			foreach ($friend_request as $row) {
				?>
				<tr>
					<td><?php echo $row->name ?></td>

					<td><a href="<?php echo site_url('tester/accept_friend/'.$row->action); ?>"><button class="btn btn-primary">Accept</button></a></td>
				</tr>
				<?php
			}
		} 
		?>
	</table>
</div>
