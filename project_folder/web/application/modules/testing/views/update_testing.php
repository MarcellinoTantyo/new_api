<div class="title">
	Update Page
</div>

<div class="container">
	<div class="content">
		<form action="<?php echo site_url('Testing/view_content/'.$view_content[0]->id); ?>" method="post">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" class="form-control" value="<?php echo($view_content[0]->name) ?>" placeholder="Name">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" value="<?php echo($view_content[0]->email) ?>" placeholder="Email">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password">
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirmation Password</label>
				<input type="password" name="confirmPassword" class="form-control" placeholder="Confirmation Password">
			</div>
			<button type="submit" class="btn btn-warning" value="update">Update</button>
		</form>
	</div>
</div>