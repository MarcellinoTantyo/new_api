<!-- carousel -->
<div class="owl-carousel owl-theme">
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
	<div class="item"><img src="<?php echo base_url('asset/images/slark_dota_2_art_95602_1024x768.jpg'); ?>"></div>
</div>



<div class="title col-xs-12">
	Learning Insert Update Delete
</div>

<div class="container">
	<div class="content">
		<form action="<?php echo site_url('Testing/crud_testing/'); ?>" method="post">
			<!-- form name -->
			<div class="form-group">
				<label for="name">Name :</label>
				<div class="text-danger"><?php echo form_error('name'); ?></div>
				<input type="text" name="name" class="form-control" placeholder="Name">
			</div>
			<!-- form email -->
			<div class="form-group">
				<label for="email">Email :</label>
				<div class="text-danger"><?php echo form_error('email'); ?></div>
				<input type="email" name="email" class="form-control" placeholder="Email">
			</div>
			<!-- form password -->
			<div class="form-group">
				<label for="password">Password :</label>
				<div class="text-danger"><?php echo form_error('password'); ?></div>
				<input type="password" name="password" class="form-control" placeholder="Password">
			</div>
			<!-- form confirm password -->
			<div class="form-group">
				<label for="confirmPassword">Confirmation Password :</label>
				<div class="text-danger"><?php echo form_error('confirmPassword'); ?></div>
				<input type="password" name="confirmPassword" class="form-control" placeholder="Confirmation Password">
			</div>
			<button type="submit" class="btn btn-success" value="insert">Add Data</button>
		</form>

		<!-- show data -->
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>

			<?php
				if (!empty($view_data)) {

				 	foreach ($view_data as $row) {
			?>
						<tr>
							<td><?php echo $row->name; ?></td>
							<td><?php echo $row->email; ?></td>
							<td>
								<a href="<?php echo site_url('Testing/view_content/'.$row->id); ?>"><button type="button" class="btn btn-warning">Update</button></a>
								<a href="<?php echo site_url('Testing/delete_data/'.$row->id); ?>"><button type="button" class="btn btn-danger">Delete</button></a>
							</td>
						</tr>
			<?php
				 	}
				}
			?>
		</table>
	</div>
</div>