<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testing extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->library(array('form_validation', 'upload'));

		$this->load->model("testing_model");
	}

	function crud_testing(){

		// validation form input data
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|max_length[12]');
		$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|trim|min_length[6]|max_length[12]|matches[password]');
		
		$file["css"] = $this->load->view("template/css/base", null, true);
		$file["css"] .= $this->load->view("template/css/carousel", null, true);

		$dataDB["view_data"] = $this->testing_model->view_data();
		
		if ($this->form_validation->run() == false) {

			$file["body"] = $this->load->view("testing", $dataDB, true);
		}else{

			$data = array(
				"name"		=> $this->input->post("name"),
				"email"		=> $this->input->post("email"),
				"password"	=> $this->input->post("password")
			);
				$this->testing_model->insert_data($data);

				$file["body"] = $this->load->view("success", null, true);
		}


		$file["js"] = $this->load->view("template/js/base", null, true);
		$file["js"] .= $this->load->view("template/js/carousel", null, true);
		$file["js"] .= $this->load->view("testing/js/carousel", null, true);

		$this->load->view("template/one_col", $file);
	}

	function delete_data($id){

		$this->testing_model->delete_data($id);
		redirect('Testing/crud_testing');
	}

	function view_content($id){

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|max_length[12]');
		$this->form_validation->set_rules('confirmPassword', 'Confirmation Password', 'required|trim|min_length[6]|max_length[12]|matches[password]');

		if ($this->form_validation->run() == false) {
			
			echo(validation_errors());

		}else{
			$data = array(
				"name"		=> $this->input->post("name"),
				"email"		=> $this->input->post("email"),
				"password"	=> $this->input->post("password")
			);

			$this->testing_model->update_data($data, $id);

			redirect('Testing/crud_testing');
		}

		$file["css"] = $this->load->view("template/css/base", null, true);

		$data["view_content"] = $this->testing_model->view_content($id);

		$file["body"] = $this->load->view("update_testing", $data, true);

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);

	}
}
?>