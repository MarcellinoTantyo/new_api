<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testing_model extends CI_Model{

	function insert_data($data){
		
		$this->load->database();//load db
		$this->db->insert('test', $data);// insert data
	}

	function view_data(){

		$this->load->database();// load db
		$this->db->select('id, name, email');// select data
		$query = $this->db->get('test');// get query
		return $query->result();// the result query

	}

	function delete_data($id){

		$this->load->database();//load db
		$this->db->select('id, name, email, password');// select data
		$this->db->where('id', $id);// where id = id yg diinput
		$this->db->delete('test');//delete data
	}

	function view_content($id){

		$this->load->database();//load db
		$this->db->select('id, name, email, password');//select db
		if(isset($id))$this->db->where('id', $id);
		$query = $this->db->get('test');//get query
		return $query->result();
	}

	function update_data($data, $id){

		$this->load->database();
		$this->db->select('id, name, email, password');
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->update('test');
	}
}