<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Language extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->library(array('form_validation', 'session'));
	}

	function select_language(){

		$lang = $this->session->lang;

		if (!empty($lang)) {
			$this->lang->load('bahasa', $lang);
		}else{
			$this->lang->load('bahasa', 'indonesian');
		}

		$file["css"] = $this->load->view("template/css/base", null, true);

		$file["body"] = $this->load->view("language", null, true);

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);
	}

	function change_language($lang){

		$this->session->set_userdata(array('lang' => $lang));
		echo $this->session->$lang;
		//if ($lang = 'Indonesian') {
		//	$this->session->get_userdata('indonesian_lang');
		//}else if ($lang = 'Deutsch') {
		//	$this->session->get_userdata('deutsch_lang');
		//}
		redirect(site_url('Language/select_language'));
	}
}
?>