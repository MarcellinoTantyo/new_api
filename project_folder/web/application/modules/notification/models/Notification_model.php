<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model{

	function register($data2){
		$this->load->database();
		$this->db->insert('notification_user', $data2);
	}

	function login($name, $password){
		$this->load->database();
		$this->db->select('user_id, name, password');
		$this->db->where('name', $name);
		$query = $this->db->get('notification_user');
		//var_dump($query->result());

		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	//insert notification
	function insert_data($data2){
		$this->load->database();
		$this->db->insert('notification', $data2);
	}

	//menampilkan data user yg sedang login
	function view_data_user($id = null){
		$this->load->database();
		$this->db->select('user_id, name');
		if(isset($id))$this->db->where('user_id', $id);
		$query = $this->db->get('notification_user');
		//var_dump($query->result());
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	//view notification
	function view_data($id = null){
		$this->load->database();
		$this->db->select('id, name, status');
		$this->db->join('notification_user', 'notification_user.user_id = notification.user_id', 'left');

		if(isset($id))$this->db->where('notification.receiver_id', $id);

		$query = $this->db->get('notification');	
		//var_dump($query->result());
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}

	function view_list_user($id = null){
		$this->load->database();
		$this->db->select('user_id, name');
		if(isset($id))$this->db->where('user_id !=', $id);

		$query = $this->db->get('notification_user');

		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	}
}