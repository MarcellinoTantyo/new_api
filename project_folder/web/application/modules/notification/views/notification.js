var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var connected = {};

http.listen(8000);

// app.get('/', function (req, res) {
//   res.sendfile(__dirname + '/Notification.php');
// });

app.get('/', function(req, res){
	res.send('Hello!!');
});

// io.on('connection', function(socket){
// 	socket.emit('news', {hello: 'world'});
// });

io.on('connection', function(socket){
  console.log('a user connected');
    socket.on('insertID', function(data){
		connected[data["id"]] = socket.id;
		console.log("connected :");
		console.log(JSON.stringify(connected));
	});
    socket.on('my_other_event', function(data){
		// io.emit('news',data);
		//looping fungsi dibawah sebanyak jumlah socket yang mau dikirim
		var parsedData = JSON.parse(data);
		// socket.broadcast.to(connected[parsedData["id"]]).emit('news', data);
		for (var i=0;i<parsedData.arrayOfChecked.length;i++){
			socket.broadcast.to(connected[parsedData['arrayOfChecked'][i]]).emit('news', data);
			console.log('sent to: '+parsedData['arrayOfChecked'][i]);
		}
		console.log(data);
	});
});

http.listen(8000, function(){
  console.log('listening on *:8000');
});
