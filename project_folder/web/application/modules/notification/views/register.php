<div class="container2">
	<div class="title">
		Registration New User
	</div>

	<div class="content4">
		<form action="<?php echo site_url('notification/register'); ?>" method="post">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" class="form-control" placeholder="Name">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-success">Register</button>
		</form>
	</div>
</div>