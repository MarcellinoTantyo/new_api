<div class="container2">
	<div class="title">
		Login
	</div>

	<div class="content4">
		<form action="<?php echo site_url('notification/login/'); ?>" method="post">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" class="form-control" placeholder="Name">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-success">Login</button>
			<?php echo $this->session->flashdata("error"); ?>
		</form>
	</div>
</div>