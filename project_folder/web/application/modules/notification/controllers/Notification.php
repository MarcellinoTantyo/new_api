<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->library(array('form_validation'));

		$this->load->model("notification_model");
	}

	function status_user(){

		$id = $this->session->userdata('user_id');

		// $this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim');

		$data["css"] = $this->load->view("template/css/base", null, true);

		$data2["view_data_user"] = $this->notification_model->view_data_user($id);
		$data2["curr_user_id"] = $id;
		$data2["view_list_user"] = $this->notification_model->view_list_user($id);

		if ($this->session->userdata('name') != '') {

			if ($this->form_validation->run() == false) {

				$data["body"] = $this->load->view("status_user", $data2, true);
				
			}else{

				$data["body"] = $this->load->view("status_user", $data2, true);

				$decodedArray = json_decode($this->input->post("arrayOfChecked"));

				$url = '127.0.0.1:9000/notifications';
				$ch = curl_init();
				for ($i=0;$i<sizeof($decodedArray);$i++){
					//extract data from the post
					//set POST variables
					$fields = array(
					        'UserName' => urlencode($this->input->post("name")),
					        'user_id' => urlencode($id),
					        'status' => urlencode($this->input->post("status")),
					        'receiver_id' => urlencode($decodedArray[$i])
					);

					$fields_string = "";

					//url-ify the data for the POST
					foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
					rtrim($fields_string, '&');

					//open connection

					//set the url, number of POST vars, POST data
					curl_setopt($ch,CURLOPT_URL, $url);
					curl_setopt($ch,CURLOPT_POST, count($fields));
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
					curl_setopt($ch, CURLOPT_TIMEOUT, 400);

					//execute post
					$result = curl_exec($ch);

					// $data2 = array(
					// 	"user_id"	=> $id,
					// 	"status"	=> $this->input->post("status"),
					// 	"receiver_id" => $decodedArray[$i]
					// );

					// $this->notification_model->insert_data($data2);
				}
				curl_close($ch);
				redirect('notification/status_user');
			}
		}else{
			redirect(site_url('notification/login'));
		}

		$data["js"] = $this->load->view("template/js/base", null, true);
		$data["js"] .= $this->load->view("template/js/socket_io", null, true);
		$data["js"] .= $this->load->view("notification/status", null, true);

		$this->load->view("template/one_col", $data);
	}

	function index(){

		$id = $this->session->userdata('user_id');

		$data["css"] = $this->load->view("template/css/base", null, true);

		$data2["curr_login_id"] = $id;
		$data2["view_data"] = $this->notification_model->view_data($id);

		$data["body"] = $this->load->view("notification", $data2, true);

		$data["js"] = $this->load->view("template/js/base", null, true);
		$data["js"] .= $this->load->view("template/js/socket_io", null, true);
		$data["js"] .= $this->load->view("notification/notif", null, true);

		$this->load->view("template/one_col", $data);
	}

	function register(){

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]');

		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|max_length[12]');

		$data["css"] = $this->load->view("template/css/base", null, true);

		if ($this->form_validation->run() == false) {
			
			$data["body"] = $this->load->view("register", null, true);

		}else{

			$option = [
				'cost' => 10,
			];

			$data2 = array(
				"name"		=> $this->input->post("name"),
				"password"	=> password_hash($this->input->post("password"), PASSWORD_BCRYPT, $option)
			);

			$this->notification_model->register($data2);

			$data["body"] = $this->load->view("success", null, true);

		}


		$data["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $data);
	}

	function test(){
		password_hash("password", PASSWORD_BCRYPT, $option);

		var_dump(password_verify('password', '$2y$10$ykCtGgSbptFNdYoMnbl1p.Ie4Wb0pztYXCFADvksvHt7AmrbP97gG'));
	}

	function login(){

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		$data["css"] = $this->load->view("template/css/base", null, true);

		if ($this->form_validation->run() == false) {

			$data["body"] = $this->load->view("login", null, true);	
		
		}else{

			$name = $this->input->post('name');
			$password = $this->input->post('password');
			//var_dump($password);
			if ($a = $this->notification_model->login($name, $password)) {
				$session_data = array(
					'user_id'	=> $a[0]->user_id,
					'name'		=> $a[0]->name,
				);
				//var_dump($a[0]->password);
				if (password_verify($password, $a[0]->password)) {
				    echo 'Password is valid!';
				    $this->session->set_userdata($session_data);
					redirect(site_url('notification/status_user'));
				} else {
				    echo 'Invalid password.';
				}

				
			}else{
				$this->session->set_flashdata('error', 'Invalid Name or Password');
				redirect(site_url('notification/login'));
			}
		}

		$data["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $data);
	}

	function session(){
		var_dump($this->session->userdata());
	}

	function logout(){

		$this->session->unset_userdata('id');
		$this->session->unset_userdata('name');
		redirect(site_url('notification/login'));
	}
}
?>