<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->library(array('form_validation', 'upload'));

		$this->load->model("upload_model");
	}

	function upload_file(){
		
		$file["css"] = $this->load->view("template/css/base", null, true);

		$data["select_data"] = $this->upload_model->select_data();
		$data["error"] = '';
		// $file["body"] = $this->load->view("upload_file", array('error'=>''), true);
		$file["body"] = $this->load->view("upload_file", $data, true);

		$file["js"] = $this->load->view("template/js/base", null, true);

		$this->load->view("template/one_col", $file);

	}

	function do_upload(){

		$config['upload_path']		= 'asset/images/';
		$config['allowed_types']	= 'gif|jpg|jpeg|png';
		$config['max_size']			= 0;
		$config['max_width']		= 0;
		$config['max_height']		= 0;

		$this->load->library('upload', $config);

		$this->upload->initialize($config);

		if (!$this->upload->do_upload('userfile')) {
			$error = array('error'=>$this->upload->display_errors());

			$this->load->view('upload_file', $error);
			//redirect(site_url('Upload/upload_file'));
		}else{
			$data = array('upload_data'	=> $this->upload->data('file_name'));

			$this->upload_model->insert_data($data);

			$this->load->view('success', $data);

		}
	}

	function delete_data($image_id){


		$this->upload_model->delete_data($image_id);
	}
}
?>