<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model{

	function insert_data($data){

		$this->load->database('');
		$this->db->insert('images', $data);
	}

	function select_data(){
		$this->load->database();
		$this->db->select('image_id, upload_data');
		$query = $this->db->get('images');
		return $query->result();
	}

	function delete_data($image_id){
		$this->load->database();
		
		//get file_name from db
		$this->db->select('image_id, upload_data');
		$this->db->where('image_id', $image_id);
		//get query
		$query = $this->db->get('images');
		//check data valid or not
		if($query->num_rows() > 0){
			//get file_name
			$result = $query->result();// return array of object
			$file_name = $result[0]->upload_data;// pilih array index pertama diambil datanya
			
			//delete file
			if (!unlink("asset/images/".$file_name)) {
				echo "Delete Error";
			}else{
				//delete record from db
				$this->db->where('image_id', $image_id);
				$this->db->delete('images');
				echo "Delete Success";
			}	
		}else{
			echo "Image not found";
			return false;
		}
		
	}
	
}