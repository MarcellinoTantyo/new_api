<div class="title">
	Upload File
</div>

<div class="container">
	<div class="content">
		<?php echo $error; ?>

		<?php echo form_open_multipart('Upload/do_upload'); ?>

		<input type="file" name="userfile" size="20">

		<br><br>

		<button type="submit" value="upload" class="btn btn-primary">Upload</button>
		
		<br><br>

		<div>
			<table class="table">
				<tr>
					<th>Image Name</th>
					<th>Action</th>
				</tr>
				
				<?php
				if (!empty($select_data)) {
					foreach ($select_data as $row) {
				?>
				<tr>
					<td><?php echo($row->upload_data); ?></td>
					<td><button class="btn btn-danger" name="delete" type="button"><a href="<?php echo site_url('Upload/delete_data/'.$row->image_id); ?>">Delete</a></button></td>
				</tr>
				<?php
					}
				}
				?>
			</table>
		</div>

	</div>
</div>