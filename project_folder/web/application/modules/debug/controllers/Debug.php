<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Debug Class
 * v 1.3
 * build 160905
 */
class Debug extends MX_Controller {

	function __construct(){
		parent::__construct();

		$this->load->library('session');
	}

	public function index(){
		echo "CI version : ".CI_VERSION."<br><br>";
		echo "<b>Session var_dump :</b>";
		echo "<pre>".var_dump($this->session->userdata())."</pre>";

		echo "<br><b>Function list :</b><br>";
		echo "<a href=".site_url('debug/php_version').">php_version()</a><br>
		get_config(\$config_name)<br>
		print_password(\$password)<br>
		check_password(\$password, \$hash)<br>";

		echo "<br><b>Database</b>:<br>";
		echo "<a href=".site_url('debug/clear_query_cache').">clear_query_cache()</a><br>";

		echo "<br><b>Session</b>:<br>";
		echo "<a href=".site_url('debug/clear_session').">clear_session()</a><br>
		set_session(\$set, \$value)<br>
		unset_session(\$set)<br>";
	}

	public function php_version(){
		phpinfo();
	}

	public function get_config($name = null){
		if(!empty($name)){
			// var_dump($this->config->item($name));
			echo "config '".$name."' : ".$this->config->item($name);
			echo "<br><a href='".site_url('debug')."'>Back</a>";
		}
		else echo "need config name";
	}

	public function clear_session(){
		$this->session->sess_destroy();
		redirect(site_url('debug'));
	}

	public function clear_query_cache(){
		$this->db->cache_delete_all();
		redirect(site_url('debug'));
	}

	public function print_password($pass, $cost = 10){
		echo password_hash($pass, PASSWORD_BCRYPT, ["cost" => $cost]);
		echo "<br>";
		echo "<a href=".site_url('debug').">Back</a>";
	}

	public function check_password($pass = 0, $hash = 0){
		if(!empty($pass) && !empty($hash)){
			if(password_verify($pass, $hash))echo "password match";
			else echo "password doesnt match";
		}
		else{
			echo "password and hash required";
		}
	}

	public function set_session($data, $value){
		$this->session->set_userdata( array( $data => $value ) );
		redirect(site_url('debug'));
	}

	public function unset_session($data){
		$this->session->unset_userdata($data);
		redirect(site_url('debug'));
	}

  /**
	* fungsi buat ngetes-tes
	* @return [type] [description]
	*/
	public function test(){
		var_dump( modules::run('auth/check_email', 'admin@admin.com'));
	}
}
