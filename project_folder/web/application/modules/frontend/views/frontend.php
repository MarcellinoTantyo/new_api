<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">
				<a href="#"><img src="<?php echo base_url('asset/images/slark.jpg'); ?>" alt="Slark"></a>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Link<span class="sr-only">(current)</span></a></li>
				<li><a href="#">Link</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Separated link</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">One more separated link</a></li>
					</ul>
				</li>
			</ul>
			<form class="navbar-form navbar-left">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Link</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown<span class="caret"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Separated link</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="title">
	Front End
</div>

<div class="container"> <!-- div body -->
	<div class="row">
		<!-- div kotak -->
		<div class="content col-xs-12 col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<img class="images" src="<?php echo base_url('asset/images/photo.jpg'); ?>" alt="Minion">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 font font2">
					DESPICABLE ME 1 & 2 & 3
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 font">
					Name			: Papoy <br>
					Blood			: AB <br>
					Favorite food	: Banana
					Sponsored by	: Despicable Me
				</div>
				<div class="col-xs-6">
					<img class="images" src="<?php echo base_url('asset/images/banana.jpg'); ?>" alt="Banana">
				</div>
			</div>
		</div>
		<div class="content col-xs-12 col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<img class="images" src="<?php echo base_url('asset/images/photo.jpg'); ?>" alt="Minion">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 font font2">
					DESPICABLE ME 1 & 2 & 3
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 font">
					Name			: Papoy <br>
					Blood			: AB <br>
					Favorite food	: Banana
					Sponsored by	: Despicable Me
				</div>
				<div class="col-xs-6">
					<img class="images" src="<?php echo base_url('asset/images/banana.jpg'); ?>" alt="Banana">
				</div>
			</div>
		</div>
		<div class="content col-xs-12 col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<img class="images" src="<?php echo base_url('asset/images/photo.jpg'); ?>" alt="Minion">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 font font2">
					DESPICABLE ME 1 & 2 & 3
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 font">
					Name			: Papoy <br>
					Blood			: AB <br>
					Favorite food	: Banana
					Sponsored by	: Despicable Me
				</div>
				<div class="col-xs-6">
					<img class="images" src="<?php echo base_url('asset/images/banana.jpg'); ?>" alt="Banana">
				</div>
			</div>
		</div>
	</div>
	<div>
		<button class="btn btn-warning tombol">Transition</button>
	</div>
</div>