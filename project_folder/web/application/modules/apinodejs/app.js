'use strict';

let http = require('http');
let server = require('./lib/server');

const PORT = process.env.port || 9000;

server.set('port', PORT);
let app = http.createServer(server);

app.listen(PORT, () => {
	console.log('Application was success');
});

//global socket.io and temp variable

global.io = require('socket.io')(app);
global.connected = {};

io.on('connection', function(socket){
  console.log('a user connected');
    socket.on('insertID', function(data){
		connected[data["id"]] = socket.id;
		console.log("connected :");
		console.log(JSON.stringify(connected));
	});
    socket.on('my_other_event', function(data){
		// io.emit('news',data);
		//looping fungsi dibawah sebanyak jumlah socket yang mau dikirim
		var parsedData = JSON.parse(data);
		// socket.broadcast.to(connected[parsedData["id"]]).emit('news', data);
		// news => send data from server to client
		// my_other_event => sent data from client to server
		for (var i=0;i<parsedData.arrayOfChecked.length;i++){
			socket.broadcast.to(connected[parsedData['arrayOfChecked'][i]]).emit('news', data);
			console.log('sent to: '+parsedData['arrayOfChecked'][i]);
		}
		console.log(data);
	});
});