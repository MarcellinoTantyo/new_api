'use strict';

let Notification = function(id, user_id, status, receiver_id){
	this.id = id;
	this.user_id = user_id;
	this.status = status;
	this.receiver_id = receiver_id;
}

module.exports = Notification;