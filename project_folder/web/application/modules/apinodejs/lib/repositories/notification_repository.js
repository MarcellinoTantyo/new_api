'use strict';

let Notification = require('../domains/notification');

let NotificationRepository = function(db){
	this.db = db;
};

NotificationRepository.prototype = {
	save: function(e, cb, errCb){
		let db = this.db;
		let data = {user_id: e.user_id, status: e.status, receiver_id: e.receiver_id};
		let query = 'INSERT INTO notification SET?';
		db.query(query, data, (err, result) => {
			if(err){
				errCb(err);
			}
			cb(result);
		});
	},

	// update: function(e, cb, errCb){
	// 	let db = this.db;
	// 	let data = [e.name, e.job, e.salary, e.code];
	// 	let query = 'UPDATE employee SET name = ?, job = ?, salary = ? WHERE code = ?';
	// 	db.query(query, data, (err, result) => {
	// 		if(err){
	// 			errCb(err);
	// 		}
	// 		cb(result);
	// 	});
	// },

	// delete: function(code, cb, errCb){
	// 	let db = this.db;
	// 	let query = 'DELETE FROM employee WHERE code = ?';
	// 	db.query(query, [code], (err, result) => {
	// 		if (err){
	// 			errCb(err);
	// 		}
	// 		cb(result);
	// 	});
	// },

	findOne: function(id, cb, errCb){
		let db = this.db;
		let query = 'SELECT * FROM notification WHERE id = ?';
		db.query(query, [id], (err, results, fields) => {
			if (err){
				errCb(err);
			}
			let result = results[0];
			if (!result){
				cb('data with id = ${id} not found..')
			} else {
				let notification = new Notification(result.id, result.user_id, result.status, result.receiver_id);
				cb(notification);
			}
		});
	},

	findAll: function(cb, errCb){
		let db = this.db;
		let query = 'SELECT * FROM  notification';
		db.query(query, (err, results, fields) => {
			if (err){
				errCb(err);
			}
			let notifications = [];
			for (let i = 0; i < results.length; i++) {
				let e = results[i];
				let notification = new Notification(e.id, e.user_id, e.status, e.receiver_id);
				notifications.push(notification);	
			}
			cb(notifications);
		});
	}
};

module.exports = NotificationRepository;