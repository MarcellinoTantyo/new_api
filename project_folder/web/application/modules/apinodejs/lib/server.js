'use strict';

let express = require('express');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');

let notificationController = require('./controllers/notification_controller');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(morgan('dev'));

app.get('/', (req, res, next) => {
	// res.send('Hello World');
	var test = JSON.stringify({msg:"World"});
	res.setHeader('Content-Type', 'application/json');
	res.send(test);
});

app.post('/notifications', notificationController.saveNotification); //untuk save
// app.put('/employees/:code', employeeController.updateEmployee); //untuk update
// app.delete('/employees/:code', employeeController.deleteEmployee); //untuk delete
app.get('/notifications/:id', notificationController.getNotification); //untuk findOne
app.get('/notifications', notificationController.getNotifications); //untuk findAll
app.get('/notif', notificationController.getNotif);

module.exports = app;