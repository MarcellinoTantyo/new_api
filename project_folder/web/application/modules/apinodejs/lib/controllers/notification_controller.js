'use strict';

let db = require('../config/mysql_config');
let NotificationRepository = require('../repositories/notification_repository');
let Notification = require('../domains/notification');

let saveNotification = (req, res, next) => {
	if (!req.body){
		next('All field must be fill...');
	}
	let data = req.body;
	let notification = new Notification(parseInt(data.id), parseInt(data.user_id), data.status, parseInt(data.receiver_id));
	let notificationRepo = new NotificationRepository(db);
	notificationRepo.save(notification, result => {
		var data2 = {"id":data.id,"name": data.UserName,"status": data.status,"arrayOfChecked":[data.receiver_id.toString()] };
		global.io.to(global.connected[data.receiver_id.toString()]).emit('news', JSON.stringify(data2));
		var result = JSON.stringify(data2);
		res.setHeader('Content-Type', 'application/json');
		res.send(result);
		next();
	}, err => {
		if (err) {
			next(err);
		}
	});
};

// let updateEmployee = (req, res, next) => {
// 	if (!req.body){
// 		next('All field must be fill...');
// 	}
// 	if (!req.params){
// 		next('Parameter does not exist..');
// 	}
// 	let data = req.body;
// 	let code = req.params.code;
// 	let employee = new Employee(code, data.name, data.job, parseFloat(data.salary));
// 	let employeeRepo = new EmployeeRepository(db);
// 	employeeRepo.update(employee, result => {
// 		res.send('Update data success');
// 		next();
// 	}, err => {
// 		if (err) {
// 			next(err);
// 		}
// 	});
// };

// let deleteEmployee = (req, res, next) => {
// 	if (!req.params){
// 		next('Parameter does not exist..');
// 	}
// 	let code = req.params.code;
// 	let employeeRepo = new EmployeeRepository(db);
// 	employeeRepo.delete(code, result => {
// 		res.send('Data has been delete');
// 		next();
// 	}, err => {
// 		if (err) {
// 			next(err);
// 		}
// 	});
// };

let getNotification = (req, res, next) => {
	if (!req.params){
		next('Parameter does not exist..');
	}
	let id = req.params.id;
	let notificationRepo = new NotificationRepository(db);
	notificationRepo.findOne(id, result => {
		res.status(200).json({message: 'get employee', data: result});
		next();
	}, err => {
		if (err){
			next(err);
		}
	});
};

let getNotifications = (req, res, next) => {
	let notificationRepo = new NotificationRepository(db);
	notificationRepo.findAll(result => {
		res.status(200).json({message: 'get all employee', data: result});
		next();
	}, err => {
		if (err){
			next(err);
		}	
	});
};

let getNotif = (req, res, next) => {
		res.status(200).json({message: 'get all employee'});
	console.log(res);
};

module.exports = {
	saveNotification: saveNotification,
	// updateEmployee: updateEmployee,
	// deleteEmployee: deleteEmployee,
	getNotification: getNotification,
	getNotifications: getNotifications,
	getNotif: getNotif
};