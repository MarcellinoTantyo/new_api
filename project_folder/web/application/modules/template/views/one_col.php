<?php
if(empty($title))$title = "ITPC";
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><?=$title?></title>
	
	<?php if(!empty($head))echo $head?>

	<?php if(!empty($css))echo $css?>
</head>
<body>
	<?=$body?>

	<?php if(!empty($js))echo $js?>
</body>