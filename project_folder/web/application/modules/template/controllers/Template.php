<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller{
	/*
	$autoload = array(
		'helper'    => array('url', 'form'),
		'libraries' => array('email'),
		);
	*/


	function __construct(){
		parent::__construct();
	}

	function admin($data){
		$this->load->view('admin', $data);
	}
}
?>