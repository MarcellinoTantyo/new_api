<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		//date_default_timezone_set("Europe/London");

		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function get_error(){
		return $this->ion_auth->errors();
	}

	/**
	 * register
	 * @param  array $register_data   (username, password, email)
	 * @param  array $additional_data
	 * @param  array $group           set user group, ex: array('1')
	 * @return mixed                  new user id or false
	 */
	public function register($register_data = null, $additional_data = null, $group = null){
		if(!empty($register_data)){
			$register = $this->ion_auth->register($register_data['username'], $register_data['password'], $register_data['email'], $additional_data, $group);

			return $register;

		}
		else{
			echo "Error, need more data";
		}
	}

	/**
	 * login
	 * @param  array $login_data ('identity', 'password', 'remember');
	 */
	function login($login_data){
		$remember = isset($login_data['remember']) ? $login_data['remember'] : false;
		
		if(!$this->ion_auth->is_max_login_attempts_exceeded($login_data['identity'])){

			if($this->ion_auth->login($login_data['identity'], $login_data['password'], $remember)){
				//login success
				$this->session->set_flashdata('auth_msg', $this->ion_auth->messages());
				return true;
			}
			else{
				// wrong identity or password
				// return $this->ion_auth->messages();
				$this->session->set_flashdata('auth_msg', $this->ion_auth->errors());
				// $this->session->set_flashdata('login_msg', 'login');
				return false;
			}
		}
		else{
			//max login attempt
			return "max_login_attempt";
		}
	}

	/**
	 * logout with ion_auth function
	 */
	function logout(){
		$this->ion_auth->logout();

		$this->session->set_flashdata('auth_msg', $this->ion_auth->messages());
		echo $this->session->flashdata('auth_msg');
		// redirect('auth')
	}

	/**
	 * change password
	 * @param  string $identity user identity, email, or username, etc
	 * @param  array $data     index: old, and new
	 * @return bool           change password status
	 */
	function change_password($data, $identity){
		if($this->check_login()){

			if(empty($identity)){
				$identity = $this->session->userdata('identity');
			}

			$change = $this->ion_auth->change_password($identity, $data['old'], $data['new']);

			if($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('auth_msg', $this->ion_auth->messages());
				return true;
			}
			else
			{
				$this->session->set_flashdata('auth_msg', $this->ion_auth->errors());
				return false;
				//redirect('auth/change_password', 'refresh');
			}
		}
	}

	/**
	 * reset password
	 * @param  string $email email to send the reset code
	 */
	public function forgot_password($email){
		$forgotten = $this->ion_auth->forgotten_password($email);

		if ($forgotten) {
			//if there were no errors
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			return $this->ion_auth->messages();
			// redirect("auth/login", 'refresh');
		}
		else {
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			return $this->ion_auth->errors();
			// redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * get user group
	 * @param  integer $id user id, if null, current user id will be used
	 * @return array     db result on user group
	 */
	public function get_user_group($id = null){
		if($this->check_login()){
			if(isset($id))return $this->ion_auth->get_users_groups($id)->result();
			else return $this->ion_auth->get_users_groups()->result();
		}
	}

	/**
	 * check user logged in or not
	 * @return bool
	 */
	public function check_login(){
		if($this->ion_auth->logged_in()){
			return true;
		}
		else{
			$this->session->set_flashdata('auth_msg', 'Login first');
			return false;
		}
	}

	/**
	 * check email exist or not
	 * @param  string $email
	 * @return bool        email exist or not
	 */
	public function check_email($email){
		if($this->ion_auth->email_check($email)){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * check identity exist or not
	 * @param  string $identity
	 * @return bool           identity exist or not
	 */
	public function check_identity($identity){
		if($this->ion_auth->identity_check($identity)){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * check user group match or not
	 * @param  integer or string $group group id or name
	 * @param  integer $id    id user, if null, current user id logged in will be used
	 * @return bool
	 */
	public function check_user_group($group, $id = null){
		// var_dump($this->ion_auth->in_group($group));
		
		return $this->ion_auth->in_group($group);
	}
	
//====================================================================================================

	public function destroy_session(){
		$this->session->sess_destroy();
	}

//====================================================================================================

}
