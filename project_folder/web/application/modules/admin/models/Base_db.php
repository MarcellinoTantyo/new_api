<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_db extends CI_Model{

	function __construct(){
		parent::__construct();

		$this->load->database();
	}

	function insert($table_name, $data){
		$this->db->insert($table_name, $data);
		// var_dump($this->db->error());
		
		if(!empty($this->db->insert_id()))
		{
			return $this->db->insert_id();
		}
		else return true;
	}

	function update($table_name, $filter, $data){
		// var_dump($filter);
		$this->db->trans_start();
		
		// $this->db->where($filter);
		$this->db->update($table_name, $data, $filter);

		$this->db->trans_complete();

		if ($this->db->trans_status() === false){
			return false;
		}
		else{
			// return $this->db->affected_rows();
			return true;
		}
	}

	function delete($table_name, $filter){
		$this->db->trans_start();
		
		$this->db->where($filter);
		$this->db->delete($table_name);

		$this->db->trans_complete();

		if ($this->db->trans_status() === false){
			return false;
		}
		else{
			// return $this->db->affected_rows();
			return true;
		}
	}

	function switch_field($table, $field, $filter){
		$this->db->set($field, $field.' * -1', FALSE);
		$this->db->where($filter);
		$this->db->update($table);

		if($this->db->affected_rows() > 0) return true;
		else return false;

	}

	/**
	 * delete file associated with database field
	 * @param  string $table  table name
	 * @param  string $field  filename field on db
	 * @param  array $filter for where query
	 * @param  string $directory file folder directory
	 */
	function delete_file_from_db($table, $field, $filter, $directory){
		//check directory
		if(substr($directory, -1) != '/') $directory .= '/';


		$this->db->select($field);
		$this->db->from($table);
		$this->db->where($filter);

		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			$result = $query->result();

			foreach ($result as $row) {
				if(!empty($row->field)){
					$file = $directory.$row->$field;
					// var_dump($file);
					if(file_exists($file)){
						unlink($file);
					}
					else{
						// echo "file not found";
						return "file not found";
					}
				}
				else{
					return "file not specified";
				}
			}
			return true;
		}
		else{
			return false;
		}
		
	}
}