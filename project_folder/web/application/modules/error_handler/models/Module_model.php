<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Module_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function test(){
		
	}


	public function select($table_name, $table_field, $condition = null){
		$this->db->select($table_field);
		$this->db->from($table_name);
		if(isset($condition)){
			//where
			if(isset($condition['where']))$this->db->where($condition['where']);
			//limit
			if(isset($condition['limit']){
				$limit = $condition['limit'];
				$offset = isset($condition['offset']) ? $condition['offset'] : 0;
				$this->db->limit($limit, $offset);
			}
		}

		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}
		else{
			return false;
		}
	}

	private function select_1(){
		
	}

	private function update($table_name, $update_data, $where){
		$this->db->trans_start();
		
		$this->db->where($where);
		$this->db->update($table_name, $update_data);

		$this->db->trans_complete();
		
		if($this->db->affected_rows() > 0)return true;
		else return false;
	}

	private function delete($table_name, $where){
		$this->db->trans_start();
		
		$this->db->where($where);
		$this->db->delete($table_name);

		$this->db->trans_complete();

		if($this->db->affected_rows() > 0) return true;
		else return false;
	}
}