<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('render'))
{
	function render($view, $data = null, $returnhtml = true){
		$CI = &get_instance();

		$view_html = $CI->load->view($view, $data, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

	/**
	 * preparing data buat jadi array di form_dropdown
	 * @param  array of object $source        hasil query
	 * @param  string $value_field   field dari hasil query yang mau jadi value
	 * @param  string $content_field field dari hasil query yang mau jadi text di tampilan
	 * @return array                array
	 */
	function prep_data_for_form_dropdown($source, $value_field, $content_field, $separator = '', $extra_content_field = null){
		if(!empty($source)){
			$result = array();
			foreach ($source as $row) {
				if($extra_content_field != null)$result[$row->{$value_field}] = $row->{$content_field} . $separator . $row->{$extra_content_field};
				else $result[$row->{$value_field}] = $row->{$content_field};
			}

			return $result;
		}
		else return "data empty";
	}

	function convert_date_to_mysql($date){
		return date('Y-m-d', strtotime(str_replace('-', '/', $date)));
	}

	function convert_date_to_web($date){
		return date('m/d/Y', strtotime($date));
	}

	function load_pagination($url, $item_count, $item_per_page){
		// var_dump($item_per_page);
		$this->load->library('pagination');

		$config['base_url'] = $url;
		$config['total_rows'] = $item_count;
		$config['per_page'] = $item_per_page;
		$config['use_page_numbers'] = TRUE;
		// $config['uri_segment'] = 0;
		// $config['num_links'] = 3;
		// $config['full_tag_open'] = '<p>';
		// $config['full_tag_close'] = '</p>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<span>';
		$config['first_tag_close'] = '</span>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<span>';
		$config['last_tag_close'] = '</span>';
		// $config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<span>';
		$config['next_tag_close'] = '</span>';
		// $config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<span>';
		$config['prev_tag_close'] = '</span>';
		$config['cur_tag_open'] = '<span class="digit current">';
		$config['cur_tag_close'] = '</span>';
		$config['num_tag_open'] = '<span class="digit">';
		$config['num_tag_close'] = '</span>';

		$this->pagination->initialize($config);

		// var_dump($this->pagination->create_links());
	}
}

?>