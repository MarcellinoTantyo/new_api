<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * default id_type dan controller yang bisa diakses tanpa session, punya page login
 * contoh:
 * $config['default_id_type'] = '1';
 * $config['default_controller'] = 'home';
 */
$config['default_id_type'] = '1';
$config['default_controller'] = 'home';

/**
 * list tipe id dengan binding default controllernya
 * $id_list = array('id_type' => 'controller_id', 1' => 'main', 'cust' => 'main2')
 * @var array
 */
$config['id_list'] = array(
	'1' => 'home',
	'2' => 'main',
	'3' => 'vendor',
	'10' => 'admin'
);

/**
 * id type yang bisa akses tanpa dicek
 * @var string
 */
$config['admin_id_type'] = '10';
?>