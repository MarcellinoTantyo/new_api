//require
var gulp = require('gulp');

var gulpif = require('gulp-if');
var changed = require('gulp-changed');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

var uglify = require('gulp-uglify');
var include = require('gulp-include');
var rubySass = require('gulp-ruby-sass');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');
var compass = require('gulp-compass');
var imagemin = require('gulp-imagemin');
var gm = require('gulp-gm');

//CONFIG
//file paths
var base = {
	src	: 'application/'
};

var baseAsset = {
	src  : 'asset/',
	dest : 'asset/'
};

var src = {
	styles : baseAsset.src + 'sass/',
	scripts: baseAsset.src + 'js/',
	images : baseAsset.src + 'images/compressed/*.{jpg,jpeg,png}',
	tojpg	: baseAsset.src + 'images/tojpg/*.png',
	codes	: base.src + '**/*.php'
};

var dest = {
	styles  : baseAsset.dest + 'css/',
	scripts : baseAsset.dest + 'js/min',
	images  : baseAsset.dest + 'images/compressed',
	tojpg	: baseAsset.dest + 'images/tojpg/'
};

//CONFIG
var print_sourcemap = true;

//sass
var sassOptions = {
	outputStyle : 'compressed',
	sourcemap : true
}

//https://www.npmjs.com/package/gulp-ruby-sass
var rubySassOptions = {
	style : 'compressed',
	sourcemap : true
}

//image
//image resize
var resizeWidth = 720;

//to jpg convert
var tojpgDelete = true;

//SCRIPTS
//uglify js
gulp.task('scripts', function(){
	gulp.src(src.scripts + '*.js')
	.pipe(plumber())
	.pipe(gulpif(print_sourcemap, sourcemaps.init()))
	.pipe(include())
	.pipe(uglify())
	.pipe(rename({
		suffix : ".min"
		}))
	.pipe(gulpif(print_sourcemap, sourcemaps.write('')))
	.pipe(gulp.dest(dest.scripts));
	});

//concat js
gulp.task('scripts-concat', function(){
	gulp.src(src.scripts)
	.pipe(plumber())
	.pupe()
	});

//STYLES
//compile sass with libsass
gulp.task('styles', function(){
	gulp.src(src.styles + '*.scss')
	.pipe(plumber())
	.pipe(gulpif(print_sourcemap, sourcemaps.init()))
	.pipe(sass(sassOptions).on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions']
		}))
	.pipe(rename({
		suffix : ".min"
		}))
	.pipe(gulpif(print_sourcemap, sourcemaps.write('')))
	.pipe(gulp.dest(dest.styles))
	.pipe(livereload());
	});

//compile sass with ruby
//perlu diupdate
gulp.task('stylesRuby', function(){
	sass(src.styles + '**/*.scss', rubySassOptions)
	.on('error', sass.logError)
	.pipe(sourcemaps.write())
	.pipe(autoprefixer({
		browsers: ['last 2 versions']
		}))
	.pipe(gulp.dest(dest.styles))
	.pipe(livereload())
	});

//IMAGE
//lossless compression
gulp.task('compressimage', function(){
	gulp.src(src.images)
	.pipe(imagemin())
	.pipe(gulp.dest(dest.images))
	});

//image resize
/*
*/
gulp.task('resizeimage', function(){
	gulp.src(src.images)
	.pipe(gm(function(gmfile){
		return gmfile.resize(resizeWidth)
		}))
	.pipe(gulp.dest(dest.images))
	});

//convert to jpg
gulp.task('tojpg', function(){
	gulp.src(src.tojpg)
	.pipe(gm(function(gmfile){
		return gmfile.setFormat('jpg');
		}))
	.pipe(gulp.dest(dest.tojpg))

	if(tojpgDelete == true){
		del(src.tojpg);
	}
	});

//WATCH
gulp.task('watch', function(){
	livereload.listen();

	gulp.watch(src.codes, livereload.reload);
	gulp.watch(src.scripts + '*.js', ['scripts']);
	gulp.watch(src.styles + '**/*.scss', ['styles']);
	});

//default
gulp.task('default', ['styles', 'watch']);

//task
gulp.task('concat', ['scriptsconcat', 'stylesconcat']);